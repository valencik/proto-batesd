package io.pig
package batesd

import cats.Applicative
import cats.effect.{Concurrent, IO, IOApp}
import cats.effect.std.Console
import cats.syntax.all._
import fansi.Bold
import fs2.{Stream, text}
import fs2.io.net.{Datagram, Network}
import com.comcast.ip4s._

object Main extends IOApp.Simple {

  val listeningPort = port"8125"
  val listenMsg = s"listening on port ${Bold.On(listeningPort.toString)}"

  def log[F[_]: Console](msg: String): F[Unit] =
    Console[F].println(s"${Bold.On("batesd")}: ${msg}")

  def parseNlog[F[_]: Console](line: String): F[Unit] =
    val res = StatsdParser.metricP.parseAll(line)
    res match {
      case Left(_)  => Console[F].println("err: " + line)
      case Right(m) => Printer.prettyPrint[F](m)
    }

  def lineOps[F[_]: Console: Applicative](line: String, filterPattern: Option[String]): F[Unit] =
    filterPattern match {
      case Some(pat) => if (line.startsWith(pat)) ().pure[F] else parseNlog(line)
      case None => parseNlog(line)
    }

  def client[F[_]: Concurrent: Console: Network](op: String => F[Unit]): F[Unit] = {
    Stream.resource(Network[F].openDatagramSocket(port = Some(listeningPort))).flatMap { socket =>
      socket.reads
        .flatMap(datagram => Stream.chunk(datagram.bytes))
        .through(text.utf8.decode)
        .intersperse("\n")
        .through(text.lines)
        .filterNot(_.isEmpty)
        .foreach { response =>
          // In and around here we could probably "join" two streams together
          // where the other stream represents the latest version of the operation to perform.
          // We could have a stream of keypresses / commands, these are transformed into the latest ops state.
          //parseNlog[F](response)
          op(response)
        }
    }.compile.drain
  }

  def run: IO[Unit] =
    log[IO](listenMsg) *> client[IO](l => lineOps(l, Some("datadog.dogstatsd.client")))
}
