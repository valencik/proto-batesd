package io.pig
package batesd

import cats.syntax.all._
import cats.parse.{Parser0, Parser => P, Numbers}
import cats.parse.Rfc5234.{alpha, digit}
import cats.effect.{IO, IOApp}


object StatsdParser {
  val dot = P.char('.')
  val bar = P.char('|')
  val col = P.char(':')
  val com = P.char(',')
  val und = P.char('_')
  val sla = P.char('/')
  val symbols      = P.oneOf(alpha :: digit :: und :: sla :: Nil).rep.string
  val symbolsNdot  = P.oneOf(alpha :: digit :: und :: sla :: dot :: Nil).rep.string
  val doubleP      = (Numbers.nonNegativeIntString ~ dot.? ~ Numbers.digits).string
  val intP         = Numbers.digits.map(_.toInt)

  val metricNameP = (((symbols.soft ~ dot).rep0.with1 ~ symbols).string <* col).map(MetricName(_))

  val valueP      = intP ~ (bar *> alpha.rep(1, 2).string) ~ ((bar ~ P.char('@')) *> doubleP).backtrack.?
  val metricValueP = valueP.mapFilter(MetricValue.fromTuple)

  val tagP        = ((symbols.string <* col) ~ symbolsNdot.string).map((n, v) => Tag(n, v))
  val tagsP       = (bar ~ P.char('#')).? *> tagP.repSep0(com)

  val metricP = (metricNameP ~ metricValueP ~ tagsP).map{case ((n, v), t) => Metric(n, v, t)}
}

object StatsyBatsy extends IOApp.Simple {
  import StatsdParser.metricP

  val m1 = "datadog.dogstatsd.client.aggregated_context:0|c|#client:java,client_version:2.11.0,client_transport:udp"
  val m2 = "poozle.active_requests:1|g|@1.000000|#version:436d93758e7ea5a755f779d5f935f18326aeed1b,environment:development"
  val m3 = "http4swdog.requests_count:1|c|@1.000000|#status_code:200,status_bucket:2xx,method:GET"
  val m4 = "http4swdog.active_requests:0|g|@1.000000"

  def debugParse[A](p: P[A], s: String): IO[Unit] =
    val res = p.parse(s)
    val warn = res match {
      case Left(err) => IO.println(s + "\n" + ("-" * err.failedAtOffset) + "^")
      case _ => IO.unit
    }
    warn *> IO.println(res)

  val run = debugParse(metricP, m4) *> IO.println(metricP.parseAll(m4).map(_.show))
}