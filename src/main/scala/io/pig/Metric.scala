package io.pig
package batesd

final case class Metric(metricName: MetricName, value: MetricValue, tags: List[Tag]):
  def show: String =
    s"${metricName.value}:${value.show}#${tags.map(_.show).mkString(",")}"

final case class MetricName(value: String) extends AnyVal

final case class Tag(name: String, value: String):
  def show: String = s"${name}:${value}"

sealed trait MetricValue(marker: String):
  def show: String = s"${marker}|${value}|"
  def value: Int
  def _marker: String = marker

final case class Distribution(value: Int, rate: Option[String]) extends MetricValue("d"):
  override def show: String = rate match {
    case Some(v) => s"d|${value}|@${v}"
    case None    => s"d|${value}|"
  }

final case class Gauge(value: Int, rate: Option[String]) extends MetricValue("g"):
  override def show: String = rate match {
    case Some(v) => s"g|${value}|@${v}"
    case None    => s"g|${value}|"
  }

final case class Counter(value: Int, rate: Option[String]) extends MetricValue("c"):
  override def show: String = rate match {
    case Some(v) => s"c|${value}|@${v}"
    case None    => s"c|${value}|"
  }

final case class Timer(value: Int)                       extends MetricValue("ms")
final case class Histogram(value: Int)                   extends MetricValue("h")
final case class Meter(value: Int)                       extends MetricValue("m")

object MetricValue:
  def fromTuple(vts: ((Int, String), Option[String])): Option[MetricValue] =
    vts match {
      case ((v, "d"), s)  => Some(Distribution(v, s))
      case ((v, "g"), s)  => Some(Gauge(v, s))
      case ((v, "c"), s)  => Some(Counter(v, s))
      case ((v, "ms"), _) => Some(Timer(v))
      case ((v, "h"), _)  => Some(Histogram(v))
      case ((v, "m"), _)  => Some(Meter(v))
      case _              => None
    }
