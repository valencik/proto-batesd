package io.pig
package batesd

import cats.effect.std.Console
import fansi.{Bold, Color}

object Printer {

  def prettyPrint[F[_]: Console](m: Metric): F[Unit] =
    val name = Bold.On(m.metricName.value)
    val value = Color.LightYellow(m.value.show)
    val tags = m.tags.map(t => Color.LightBlue(t.name) ++ s":${t.value}").mkString(", ")
    val line = List(name, value, tags).mkString("  ")
    Console[F].println(line)
}
