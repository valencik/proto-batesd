#!/bin/bash
cs bootstrap \
	--force \
	--standalone \
	--main-class io.pig.batesd.Main \
	--output batesd \
	io.pig:batesd_3.0.0-RC3:0.1.0-SNAPSHOT
