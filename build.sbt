ThisBuild / organization := "io.pig"
ThisBuild / scalaVersion := "3.0.2"
ThisBuild / versionScheme := Some("early-semver")

val catsVersion            = "2.6.1"
val catsEffectVersion      = "3.2.9"
val catsParseVersion       = "0.3.4"
val fansiVersion           = "0.2.14"
val fs2Version             = "3.1.3"
val ip4sVersion            = "3.0.4"
val munitCatsEffectVersion = "1.0.5"

lazy val root = (project in file(".")).settings(
  name := "batesd",
  libraryDependencies ++= Seq(
    // "core" module - IO, IOApp, schedulers
    // This pulls in the kernel and std modules automatically.
    "org.typelevel" %% "cats-effect" % catsEffectVersion,
    // concurrency abstractions and primitives (Concurrent, Sync, Async etc.)
    "org.typelevel" %% "cats-effect-kernel" % catsEffectVersion,
    // standard "effect" library (Queues, Console, Random etc.)
    "org.typelevel" %% "cats-effect-std" % catsEffectVersion,

    "co.fs2" %% "fs2-core" % fs2Version,
    "co.fs2" %% "fs2-io" % fs2Version,
    "com.comcast" %% "ip4s-core" % ip4sVersion,
    "org.typelevel" %% "cats-parse" % catsParseVersion,
    "com.lihaoyi" %% "fansi" % fansiVersion,

    "org.typelevel" %% "munit-cats-effect-3" % munitCatsEffectVersion % Test
  )
)
